from sunsynk import AMPS, CELSIUS, KWH, VOLT, WATT
from sunsynk.rwsensors import NumberRWSensor, SelectRWSensor, TimeRWSensor
from sunsynk.sensors import (
    MathSensor,
    Sensor,
    SensorDefinitions,
    TempSensor,
)

SENSORS = SensorDefinitions()

SENSORS += NumberRWSensor(206, "System Zero Export power", WATT, -1, min=-100, max=100)
